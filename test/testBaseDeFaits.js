const assert = require('assert')

const Fait = require('../app/Application/Fait')
const BaseDeFait = require('../app/Application/BaseDeFaits')

describe('BaseDeFait', () => {

    describe('#initialiser(faits)', () => {
        it('doit ajouter les faits', () => {
            const fait1 = new Fait('fait1', 0)
            const fait2 = new Fait('fait2', 1)
    
            const faits = [fait1, fait2]
    
            const baseDeFait = new BaseDeFait()
            baseDeFait.initialiser(faits)
    
            assert.equal(baseDeFait.faits.length, faits.length, `Initialisation avec ${faits.length} faits mais nous en avons ${baseDeFait.faits.length} à la fin.`)    
        })
    })

    describe('#ajouterFait(fait)', () => {
        it('doit ajouter le bon nombre de fait', () => {
            const fait = new Fait('fait', 0)

            const baseDeFait = new BaseDeFait()

            const nbFaitsAvant = baseDeFait.faits.length
            baseDeFait.ajouterFait(fait)

            assert.equal(nbFaitsAvant + 1, baseDeFait.faits.length, `Avant ajout: ${nbFaitsAvant}, après ajout: ${baseDeFait.faits.length}`)
        })
    })

    describe('#retirerFait(nom)', () => {
        it('doit retirer le bon nombre d\'élément', () => {
            const fait1 = new Fait('fait1', 1)

            const baseDeFait = new BaseDeFait()
            baseDeFait.ajouterFait(fait1)
            const nbFaitsAvant = baseDeFait.faits.length
    
            baseDeFait.retirerFait('fait1')
    
            assert.notEqual(nbFaitsAvant, baseDeFait.faits.length, `Avant: ${nbFaitsAvant}, après: ${baseDeFait.faits.length}`)
        })
    })

    describe('#nettoyer()', () => {
        it('doit enlever touts les faits de la base', () => {
            const nbFaitsAjouter = 5
            const baseDeFait = new BaseDeFait()
            for (let i = 0; i < nbFaitsAjouter; i++) {
                baseDeFait.ajouterFait(new Fait(`fait${i}`,0))               
            }

            baseDeFait.nettoyer()
            const nbFaitsApres = baseDeFait.faits.length

            assert.equal(nbFaitsApres, 0, `La base de faits comporte encore ${baseDeFait.faits.length} faits au lieu de 0.`)
        })
    })

    describe("#get(nom)", () => {
        it('doit renvoyer le bon fait', () => {
            const fait1 = new Fait('triangle', 0)
            const bf = new BaseDeFait()
            bf.ajouterFait(fait1)
    
            const f = bf.get('triangle')
    
            assert.equal(f, fait1, `Le fait n'est pas bon.`)
        })
    })

    describe('#afficher()', () => {
        
        it('doit retourner je ne sais pas lorsque la base est vide', () => {
            const bf = new BaseDeFait()

            const EXPECTED = "Je ne sais pas"

            const retour = bf.afficher()

            assert.equal(retour, EXPECTED, `Doit afficher: ${EXPECTED}; et affiche: ${retour}`)
        })

    })

    describe('#afficherLePlusHaut()', () => {
        it('doit retourner le fait avec le plus haut niveau', () => {
            const nbFaitsAjouter = 5
            const bf = new BaseDeFait()
            for(let i = 0; i < nbFaitsAjouter; i++) {
                bf.ajouterFait(new Fait(`fait${i}`,i))
            }
    
            const plusHaut = bf.afficherLePlusHaut()
            
            assert.equal(plusHaut, bf.getFaits()[bf.getFaits().length-1].nom, `TODO`)
        })
    })
})