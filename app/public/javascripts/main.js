function validation(nbCote, nbAngleDroit, nbCoteMemeLongueur, nbCoteParallele) {

    let isValid = true;
    let message = '';

    if(nbCote < 3 || nbCote > 5) {
        isValid = false;
        message = "Erreur de connaissance: Le nombre de côté doit être entre 3 et 5"
    }
    if(nbCote == 3 && nbAngleDroit > 1) {
        isValid = false;
        message = "Erreur de forme: Un triangle ne peux avoir au maximum qu'un angle droit"
    }
    if(nbCote < nbCoteMemeLongueur || nbCote < nbCoteParallele) {
        isValid = false;
        message = "Erreur de forme: Votre forme ne peux pas avoir plus de côtés parallèles ou égaux que sont nombre de côtés"
    }
    if(nbCote < nbAngleDroit) {
        isValid = false;
        message = "Erreur de forme: Il y a plus d'angle droit dans votre forme que de côtés"
    }
    if(nbCote == 3 && nbCoteParallele > 0) {
        isValid = false;
        message = "Erreur de forme: Il ne peut y avoir de côtés parallèles pour un triangle (peu importe ses caractéristiques)";
    }

    return { isValid: isValid, message: message}
}


$(document).ready( () => {
    $('#run').click( () => {
        let message = $('#message');
        message.empty();
        const nbCote = parseInt($('#nb_cote').val()) || 0;
        const nbAngleDroit = parseInt($('#nb_angle_droit').val()) || 0;
        const nbCoteMemeLongueur = parseInt($('#nb_meme_longeur').val()) || 0;
        const nbCoteParallele = parseInt($('#nb_parrallele').val()) || 0;


        const v = validation(nbCote, nbAngleDroit, nbCoteMemeLongueur, nbCoteParallele);
        console.log(v.isValid);
        
        if(v.isValid) {
            $.post('/solve', {nbCote: nbCote, nbAngleDroit: nbAngleDroit, nbCoteMemeLongueur: nbCoteMemeLongueur, nbCoteParallele: nbCoteParallele}, (result => {
                message.text(result)
            }))
        } else {
            message.text(v.message)
        }
       
        
    })
})


