const fs = require('fs')

class BaseDeFaits {

    constructor() {
        this.faits = []
    }

    initialiser(faits) {
        this.faits = faits
    }

    ajouterFait(fait) {
        this.faits.push(fait)
    }

    retirerFait(nom) {
        const fait = this.faits.find( fait => {return fait.nom == nom})
        if(fait) {
            this.faits.splice(this.faits.indexOf(fait), 1)
        } else {
            throw new Error(`Aucun fait trouvé pour le nom ${nom}`)
        }
    }

    nettoyer() {
        this.faits = []
    }

    get(nom) {
        return this.faits.find( fait => { return fait.nom == nom})
    }

    afficher() {
        return this.faits.length > 0 ? this.faits.map( f => {return `${f.nom} (${f.niveau})`}).join(", ") : "Je ne sais pas"
    }

    afficherLePlusHaut() {        
        const tableauTrie = this.getFaits().sort( (fait, ancienFait)  => {            
            return fait.niveau - ancienFait.niveau            
        })
        return tableauTrie[tableauTrie.length-1].nom
    }

    getFaits() {
        return this.faits
    }

    getDonnees() {
        try {
            return JSON.parse(fs.readFileSync('./app/Application/baseDeFaits.json', 'utf-8'))
        } catch(error) {
            console.error('Impossible de récupérer les données.', error.message);
            
        }
    }

    enregistrerEnBase() {
        const donnees = this.getDonnees()
        donnees.push({'faits': this.faits.filter(f => f.niveau == 0), 'conclusion': this.afficherLePlusHaut()})
        try {
            fs.writeFileSync('./app/Application/baseDeFaits.json', JSON.stringify(donnees, null, 2))
        } catch (error) {
            console.error('Impossible d\'écrire dans le fichier.', error.message);
        }
    }

    existeDeja() {
        const donnees = this.getDonnees()

        let res = null

        donnees.forEach( (donnee, n) => {
            console.log(`Scan de la base en cours... ${Math.round(n*100/donnees.length)}% \r`);

            const match = donnee.faits.every( (f,i) => {
                return f.nom == this.faits[i].nom && f.niveau == this.faits[i].niveau
            })

            if(match) {                
                res = donnee.conclusion
            }  
        })

        return res
        
        
    }

}

module.exports = BaseDeFaits